Rodando a aplicacao
```
ng serve
```
Criando projeto com angular2
``` bash
ng new primeiro-projeto
```
Criar component com @angular/cli
```
ng g c meu-component
```
Compilar arquivo typescript para gerar arquivo javascript
``` 
tsc arquivo.ts
```
Tipagem de variaveis de typescript para js
"any", permite a  alteracao de tipo da variavel
```
var n1: any = "sasas";
n1 = 4;
```
Criando modulo com @angular/cli
```
ng g module cusros
```
Apos criar app.module cria compnentes
```
ng g component cursos
```
Criando componente dentro da  pasta cursos
```
ng g c cursos/surso-detalhe
```
Criando serviços com @angular/cli
```
ng g service cursos/cursos
