var minhaVar = "Minha variavel";
function minhaFunc(x, y) {
    return x + y;
}
// ES6 ou ES 2015
var num = 2;
var PI = 3.14;
var numeros = [1, 2, 3];
numeros.map(function (valor) {
    return valor * 2;
});
//arrow functions
numeros.map(function (valor) { return valor * 2; });
var Metematica = /** @class */ (function () {
    function Metematica() {
    }
    Metematica.prototype.soma = function (x, y) {
        return x + y;
    };
    return Metematica;
}());
var n1 = "sasas";
n1 = 4;
