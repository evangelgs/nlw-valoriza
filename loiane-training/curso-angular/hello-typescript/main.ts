var minhaVar = "Minha variavel";

function minhaFunc(x, y){
    return x + y;
}

// ES6 ou ES 2015
let num = 2;
const PI = 3.14;

var numeros = [1, 2, 3];
numeros.map(function(valor){
    return valor * 2;
});

//arrow functions
numeros.map(valor => valor * 2 );

class Metematica {
    soma(x, y){
        return x + y;
    }
}
// tipagem "any"
var n1: any = "sasas";
n1 = 4;