import { Component, OnInit } from '@angular/core';

import { CursoService } from './curso.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  nomePortal: string;
  cursos: string[];

  constructor(private cursoService: CursoService) { 
    this.nomePortal = 'http://loiane.training.com.br';
    
    /*for (let i = 0; i < this.cursos.length; i++) {
      let curso = this.cursos[i];
    }*/
    //var servico = new CursoService();
    this.cursos = this.cursoService.getCursos();

  }
    
  ngOnInit(): void {}
}
