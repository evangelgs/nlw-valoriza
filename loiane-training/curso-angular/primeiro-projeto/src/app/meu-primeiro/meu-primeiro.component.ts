import { Component } from "@angular/core";

// class js, usar decorator importado 
// de @angular/core para angular2
@Component({
    selector: "meu-primeiro-component",
    template: `
        <p>Meu Primeiro component com Angular 2</p>
    `
}) 
export class MeuPrimeiroComponent {
    
}